// Copyright 2021 Zheleztsov N. (ML-12)

/*
 * Implement a queue using two stacks.
 * A stack must be implemented with a dynamic buffer
 */

#include <assert.h>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <limits>
#include <memory>
#include <stdexcept>
#include <type_traits>

//////////////////////////////////////////
//               STACK                  //
//////////////////////////////////////////

// Based on Vector implementation:
// https://github.com/NikZheleztsov/Vector-implementation

template <class T, class Allocator = std::allocator<T> >
class Stack {
  T *_data = nullptr;
  size_t _size = 0;
  size_t _cap = 0;
  Allocator _alloc = Allocator();

  void reallocate();

 public:
  typedef T value_type;
  typedef Allocator allocator_type;
  typedef size_t size_type;
  typedef value_type &reference;
  typedef const value_type &const_reference;
  typedef typename std::allocator_traits<Allocator>::pointer pointer;
  typedef
      typename std::allocator_traits<Allocator>::const_pointer const_pointer;
  typedef typename std::allocator_traits<Allocator> alloc_traits;

  ////////////////////////
  //  Member functions  //
  ////////////////////////

  // Constructors
  Stack();
  Stack(const Stack &other);
  Stack(Stack &&other);
  template <class InputIt>
  Stack(InputIt first, InputIt last);

  // Operator =
  Stack &operator=(const Stack &other);
  Stack &operator=(Stack &&other);

  // Element access
  reference top() { return _data[_size - 1]; }

  // Capacity
  bool empty() const { return _size == 0; }
  size_type size() const { return _size; }
  size_type max_size() const;

  // Modifiers
  void push(const T &value);
  void pop();
  void swap(Stack &other);

  // Destructor
  ~Stack();
};

template <class T, class Allocator>
Stack<T, Allocator>::Stack() : _size(0), _cap(4) {
  _data = Stack::alloc_traits::allocate(_alloc, _cap);
}

template <class T, class Allocator>
inline void Stack<T, Allocator>::reallocate() {
  T *temp = _data;
  size_t old_cap = _cap;

  (_cap % 2 == 0) ? _cap = 1.5 *_cap : _cap = 1.5 * _cap + 1;

  _data = std::allocator_traits<Allocator>::allocate(_alloc, _cap);

  if (std::is_trivially_copyable<T>::value)
    std::memcpy(_data, temp, _size * sizeof(T));
  else
    for (Stack::size_type i = 0; i < _size; i++)
      Stack::alloc_traits::construct(_alloc, _data + i, temp[i]);

  Stack::alloc_traits::deallocate(_alloc, temp, old_cap);
}

template <class T, class Allocator>
inline Stack<T, Allocator>::Stack(const Stack &other)
    : _size(other._size), _cap(other._cap) {
  _data = Stack::alloc_traits::allocate(_alloc, other._cap);

  if (std::is_trivially_copyable<T>::value)
    std::memcpy(_data, other._data, other._size * sizeof(T));
  else
    for (Stack::size_type i = 0; i < other._size; i++)
      Stack::alloc_traits::construct(_alloc, _data + i, other._data[i]);
}

template <class T, class Allocator>
inline Stack<T, Allocator>::Stack(Stack &&other) {
  _size = other._size;
  _cap = other._cap;
  _data = other._data;
  other._data = nullptr;
}

template <class T, class Allocator>
template <class InputIt>
inline Stack<T, Allocator>::Stack(InputIt first, InputIt last) {
  auto dist = last - first;  // can't be < 0
  auto max_size = Stack::alloc_traits::max_size(_alloc);
  if (dist < 0 || dist > max_size)
    throw std::length_error("cannot create stack");

  _data = Stack::alloc_traits::allocate(_alloc, dist);

  for (Stack::size_type i = 0; i < dist; ++i, ++first)
    Stack::alloc_traits::construct(_alloc, _data + i, *first);

  _size = std::abs(dist);
  _cap = std::abs(dist + 1);
}

template <class T, class Allocator>
inline Stack<T, Allocator> &Stack<T, Allocator>::operator=(const Stack &other) {
  Stack::alloc_traits::deallocate(_alloc, _data, _cap);
  _data = Stack::alloc_traits::allocate(_alloc, other._cap);

  if (std::is_trivially_copyable<T>::value)
    std::memcpy(_data, other._data, other._size * sizeof(T));
  else
    for (Stack::size_type i = 0; i < _size; i++)
      Stack::alloc_traits::construct(_alloc, _data + i, other._data[i]);

  _size = other._size;
  _cap = other._cap;

  return *this;
}

template <class T, class Allocator>
inline Stack<T, Allocator> &Stack<T, Allocator>::operator=(Stack &&other) {
  _size = other._size;
  _cap = other._cap;
  _data = other._data;
  other._data = nullptr;

  return *this;
}

template <class T, class Allocator>
inline typename Stack<T, Allocator>::size_type Stack<T, Allocator>::max_size()
    const {
  const size_t dif_max = std::numeric_limits<std::ptrdiff_t>::max();
  const size_t aloc_max = Stack::alloc_traits::max_size(_alloc);
  return std::min(dif_max, aloc_max);
}

template <class T, class Allocator>
void Stack<T, Allocator>::push(const T &value) {
  if (_size + 1 > _cap) reallocate();

  Stack::alloc_traits::construct(_alloc, _data + _size, value);
  _size++;
}

template <class T, class Allocator>
inline void Stack<T, Allocator>::pop() {
  Stack::alloc_traits::destroy(_alloc, _data + _size - 1);
  _size--;
}

template <class T, class Allocator>
inline void Stack<T, Allocator>::swap(Stack &other) {
  Stack other_temp(std::move(other));
  other = std::move(*this);
  *this = std::move(other_temp);
}

template <class T, class Allocator>
inline Stack<T, Allocator>::~Stack() {
  Stack::alloc_traits::deallocate(_alloc, _data, _cap);
}

//////////////////////////////////////////
//               QUEUE                  //
//////////////////////////////////////////

template <class T>
class Queue {
  Stack<T> _push_st, _pop_st;
  size_t _size = 0;

  void move_push_to_pop();

 public:
  typedef Stack<T> container;
  typedef typename Stack<T>::value_type value_type;
  typedef typename Stack<T>::size_type size_type;
  typedef typename Stack<T>::reference reference;
  typedef typename Stack<T>::const_reference const_reference;

  ////////////////////////
  //  Member functions  //
  ////////////////////////

  // Constructors
  Queue() = default;
  Queue(const Queue &other);
  Queue(Queue &&other);
  template <class InputIt>
  Queue(InputIt first, InputIt last);

  // Operator =
  Queue &operator=(const Queue &other);
  Queue &operator=(Queue &&other);

  // Element access
  reference front();
  reference back() { return _push_st.top(); }

  // Capacity
  bool empty() const { return _size == 0; }
  size_type size() const { return _size; }

  // Modifiers
  void push(const value_type &value) {
    _push_st.push(value);
    ++_size;
  }
  void pop();

  // Destructor
  ~Queue() = default;
};

template <class T>
void Queue<T>::move_push_to_pop() {
  while (!_push_st.empty()) {
    _pop_st.push(_push_st.top());
    _push_st.pop();
  }
}

template <class T>
Queue<T>::Queue(const Queue &other)
    : _push_st(other._push_st), _pop_st(other._pop_st), _size(other._size) {}

template <class T>
Queue<T>::Queue(Queue &&other)
    : _push_st(std::move(other._push_st)),
      _pop_st(std::move(other._pop_st)),
      _size(other._size) {}

template <class T>
template <class InputIt>
Queue<T>::Queue(InputIt first, InputIt last) {
  auto dist = last - first;
  auto max_size = _pop_st.max_size() * 2;
  if (dist < 0 || dist > max_size)
    throw std::length_error("cannot create stack");

  auto mid = dist / 2;

  for (auto i = first + mid - 1; i >= first; --i) _pop_st.push(*i);

  for (auto i = first + mid; i < last; ++i) _push_st.push(*i);

  _size = dist;
}

template <class T>
Queue<T> &Queue<T>::operator=(const Queue<T> &other) {
  _pop_st = other._pop_st;
  _push_st = other.push_st;
  _size = other._size;
  return *this;
}

template <class T>
Queue<T> &Queue<T>::operator=(Queue<T> &&other) {
  _pop_st = std::move(other._pop_st);
  _push_st = std::move(other.push_st);
  _size = other._size;
  return *this;
}

template <class T>
typename Queue<T>::reference Queue<T>::front() {
  if (_pop_st.empty() && !_push_st.empty()) move_push_to_pop();

  return _pop_st.top();
}

template <class T>
void Queue<T>::pop() {
  if (_pop_st.empty() && !_push_st.empty()) move_push_to_pop();

  _pop_st.pop();
  --_size;
}

int main() {
  Queue<int> que;
  uint32_t num_of_commands = 0;
  std::cin >> num_of_commands;

  bool is_OK = true;
  for (uint32_t i = 0; i < num_of_commands; ++i) {
    int a = 0, b = 0, temp = 0;
    std::cin >> a >> b;
    switch (a) {
      case (2): {
        if (que.empty())
          temp = -1;
        else {
          temp = que.front();
          que.pop();
        }

        if (b != temp) is_OK = false;

        break;
      }

      case (3):
        que.push(b);
        break;

      default:
        assert(false);
    }
  }

  std::cout << (is_OK ? "YES" : "NO");
  return 0;
}
