// Copyright 2021 Zheleztsov N. (ML-12)

// Variant № 4
// Implement the “random element” pivot selection strategy.
// Implement the Partition function by traversing two iterators
// from the end of the array to the beginning.

#include <assert.h>

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <future>
#include <iostream>

template <typename T, class Compare = std::greater<T>>
int partition(T *arr, size_t l, size_t r, Compare comp = std::greater<T>()) {
  int rand_pivot_pos = (r > l) ? std::rand() % (r - l) + l : r;
  if (r != rand_pivot_pos) {
    std::swap(arr[r], arr[rand_pivot_pos]);
  }

  int i = l, j = l;
  while (j < r) {
    if (comp(arr[j], arr[r])) {
      ++j;
    } else {
      std::swap(arr[i++], arr[j++]);
    }
  }

  std::swap(arr[i], arr[r]);
  return i;
}

template <typename T, class Compare = std::greater<T>>
int k_stat(T *arr, size_t l, size_t r, size_t p,
           Compare comp = std::greater<T>()) {
  size_t k = ((p / 100.0) * (r + 1));
  int pivot_pos = partition(arr, l, r, comp);

  while (pivot_pos != k) {
    if (k < pivot_pos) {
      r = pivot_pos - 1;
      pivot_pos = partition(arr, l, r, comp);
    } else {
      l = pivot_pos + 1;
      pivot_pos = partition(arr, l, r, comp);
    }
  }

  return arr[k];
}

int main(int argc, char **argv) {
  std::srand(std::time(0));

  size_t size = 0;
  std::cin >> size;
  int *arr = new int[size];

  for (size_t i = 0; i < size; ++i) std::cin >> arr[i];

  std::cout << k_stat(arr, 0, size - 1, 10) << std::endl
            << k_stat(arr, 0, size - 1, 50) << std::endl
            << k_stat(arr, 0, size - 1, 90) << std::endl;

  delete[] arr;
  return 0;
}
